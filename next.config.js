/** @type {import('next').NextConfig} */
const nextConfig = {
  // Configuración de exportación
  output: "export",

  // Opcional: Cambiar enlaces `/me` -> `/me/` y generar `/me.html` -> `/me/index.html`
  // trailingSlash: true,

  // Opcional: Prevenir el redireccionamiento automático `/me` -> `/me/`, en lugar de preservar `href`
  // skipTrailingSlashRedirect: true,

  // Opcional: Cambiar el directorio de salida `out` -> `dist`
  // distDir: 'dist',
};

module.exports = nextConfig;
