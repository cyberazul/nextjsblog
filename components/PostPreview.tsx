// Importa el componente Link de la biblioteca Next.js para gestionar la navegación entre páginas
import Link from "next/link";

// Importa el componente PostMetadata que se espera recibir como propiedades
import { PostMetadata } from "./PostMetadata";

// Define el componente funcional PostPreview que renderiza una vista previa de un post
const PostPreview = (props: PostMetadata) => {
  return (
    // Contenedor principal con estilos de diseño utilizando Tailwind CSS
    <div className="border border-slate-300 p-4 rounded-md shadow-sm bg-white">
      {/* Muestra la fecha del post */}
      <p className="text-sm text-slate-400">{props.date}</p>

      {/* Utiliza el componente Link para crear un enlace a la página del post completo */}
      <Link href={`/posts/${props.slug}`}>
        {/* Título del post con estilos y efecto de subrayado al pasar el ratón (hover) */}
        <h2 className="text-tdr hover:underline mb-4">{props.title}</h2>
      </Link>

      {/* Muestra el subtítulo del post */}
      <p className="text-slate-700">{props.subtitle}</p>
    </div>
  );
};

// Exporta el componente PostPreview para que pueda ser utilizado en otros archivos
export default PostPreview;
