// Define una interfaz llamada PostMetadata para describir la estructura de los metadatos de un post
export interface PostMetadata {
  // Propiedad que representa el título del post, se espera que sea una cadena de texto
  title: string;

  // Propiedad que representa la fecha del post, se espera que sea una cadena de texto
  date: string;

  // Propiedad que representa el subtítulo del post, se espera que sea una cadena de texto
  subtitle: string;

  // Propiedad que representa el slug del post, se espera que sea una cadena de texto
  slug: string;
}
