// Importa el módulo 'fs' para trabajar con el sistema de archivos
import fs from "fs";

// Importa el módulo 'gray-matter' para procesar archivos Markdown y extraer metadatos
import matter from "gray-matter";

// Importa la interfaz PostMetadata definida en otro archivo
import { PostMetadata } from "../components/PostMetadata";

// Función que obtiene metadatos de los posts en formato de array de objetos PostMetadata
const getPostMetadata = (): PostMetadata[] => {
  // Ruta al directorio que contiene los archivos de los posts
  const folder = "posts/";

  // Lee los nombres de los archivos en el directorio de posts
  const files = fs.readdirSync(folder);

  // Filtra los archivos para incluir solo los archivos con extensión ".md" (Markdown)
  const markdownPosts = files.filter((file) => file.endsWith(".md"));

  // Obtiene los datos de gray-matter de cada archivo y los mapea a objetos PostMetadata
  const posts = markdownPosts.map((fileName) => {
    // Lee el contenido del archivo Markdown
    const fileContents = fs.readFileSync(`posts/${fileName}`, "utf8");

    // Procesa el contenido del archivo con gray-matter para obtener los metadatos
    const matterResult = matter(fileContents);

    // Retorna un objeto PostMetadata con los metadatos extraídos
    return {
      title: matterResult.data.title,
      date: matterResult.data.date,
      subtitle: matterResult.data.subtitle,
      slug: fileName.replace(".md", ""), // El slug se obtiene del nombre del archivo sin la extensión
    };
  });

  // Retorna el array de objetos PostMetadata que contiene los metadatos de los posts
  return posts;
};

// Exporta la función getPostMetadata para que pueda ser utilizada en otros archivos
export default getPostMetadata;
