/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./app/**/*.{js,ts,jsx,tsx}", "./components/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        tdr: "rgb(183, 0, 40)",
      },
    },
  },
  plugins: [require("@tailwindcss/typography")],
};
