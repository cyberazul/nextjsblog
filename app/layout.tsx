import "../styles/globals.css";

// Definir el componente funcional RootLayout que recibe children como prop
export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html>
      <body>
        {/* Contenedor principal con ancho máximo y relleno */}
        <div className="mx-auto max-w-2x1 px-6">{children}</div>
      </body>
    </html>
  );
}
