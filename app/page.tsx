// Importa la función getPostMetadata que obtiene metadatos de los posts
import getPostMetadata from "../components/getPostMetadata";

// Importa el componente PostPreview para mostrar las vistas previas de los posts
import PostPreview from "../components/PostPreview";

// Define el componente funcional HomePage que representa la página principal
const HomePage = () => {
  // Obtiene los metadatos de los posts llamando a la función getPostMetadata
  const postMetadata = getPostMetadata();

  // Mapea los metadatos de los posts a componentes PostPreview y los almacena en un array
  const postPreviews = postMetadata.map((post) => (
    // Usa el componente PostPreview y asigna un key único basado en el slug del post
    <PostPreview key={post.slug} {...post} />
  ));

  // Renderiza un contenedor con una rejilla de 1 o 2 columnas dependiendo del tamaño de la pantalla
  return (
    <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
      {/* Renderiza las vistas previas de los posts */}
      {postPreviews}
    </div>
  );
};

// Exporta el componente HomePage para que pueda ser utilizado en otros archivos
export default HomePage;
