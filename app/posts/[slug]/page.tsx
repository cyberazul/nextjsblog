// Importa módulos y componentes necesarios
import fs from "fs";
import Markdown from "markdown-to-jsx";
import matter from "gray-matter";
import getPostMetadata from "../../../components/getPostMetadata";
import "./post.css";

// Función para obtener el contenido de un post dado su slug
const getPostContent = (slug: string) => {
  // Ruta al directorio que contiene los archivos de los posts
  const folder = "posts/";

  // Construye la ruta completa al archivo del post utilizando el slug
  const file = `${folder}${slug}.md`;

  // Lee el contenido del archivo Markdown
  const content = fs.readFileSync(file, "utf8");

  // Procesa el contenido del archivo con gray-matter para obtener los metadatos y el contenido
  const matterResult = matter(content);

  // Retorna el resultado de gray-matter que contiene tanto los metadatos como el contenido
  return matterResult;
};

// Función asincrónica para generar parámetros estáticos para las páginas estáticas generadas
export const generateStaticParams = async () => {
  // Obtiene los metadatos de los posts llamando a la función getPostMetadata
  const posts = getPostMetadata();

  // Mapea los slugs de los posts y retorna un array de objetos con la estructura { slug: string }
  return posts.map((post) => ({
    slug: post.slug,
  }));
};

// Componente funcional PostPage que representa la página individual de un post
const PostPage = (props: any) => {
  // Obtiene el slug del post de los parámetros de la ruta
  const slug = props.params.slug;

  // Obtiene el contenido del post llamando a la función getPostContent con el slug
  const post = getPostContent(slug);

  // Renderiza la página del post con título, fecha y contenido Markdown
  return (
    <div>
      <div className="my-12 text-center">
        {/* Título del post */}
        <h1 className="text-2xl text-slate-600 ">{post.data.title}</h1>

        {/* Fecha del post */}
        <p className="text-slate-400 mt-2">{post.data.date}</p>
      </div>

      {/* Contenido del post renderizado como Markdown */}
      <article className="post">
        <Markdown>{post.content}</Markdown>
      </article>
    </div>
  );
};

// Exporta el componente PostPage para que pueda ser utilizado en otros archivos
export default PostPage;
